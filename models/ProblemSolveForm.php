<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "problem".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $timestamp
 * @property int $idUser
 * @property string $status
 *
 * @property User $idUser0
 */
class ProblemSolveForm extends Problem
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reason'], 'required'],
            // [['description', 'status'], 'string'],
            // [['timestamp'], 'safe'],
            // [['idUser', 'idCategory'], 'integer'],
            // [['name', 'photoBefore', 'photoAfter'], 'string', 'max' => 255],
            // [['idCategory'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['idCategory' => 'id']],
            // [['idUser'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['idUser' => 'id']],
        ];
    }


}
